Blog :
----
- Post
- Commentaire
- Tag
- Utilisateur

Besoin/Construction :
---------------------
Base donnée
Front client
Back admin


Base donnée
-----------
voir schema

Front:
------
- Page d'acceuil
    - 5 derniers messages (juste titre résumer) ordre decroisant de date publication
- Pages Tag
    - 5 derniers messages du tag (juste résumer) ordre decroisant de date publication
- Page message
    - affiche le message complet
    - Formulaire commentaire
    - Contenue des commentaire.
- Page liste tag
    - Liste les tag avec un lien vers liste de leur messages
- Menu
    - liste lien :
        - lien acceuil
        - liens des tag choisie
        - lien liste tag

Back :
------
- login
- création utilisateur.
- penser securité
- gestion messages (liste/modifier/créer)
- gestion utilisateur + affectation droit
- gestion tag (+/-/liste)


Stack :
-------
Front : HTML/Bootstrap
Back : PHP/SF
BDD : SQLLite

