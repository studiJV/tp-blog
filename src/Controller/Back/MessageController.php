<?php


namespace App\Controller\Back;


use App\Entity\Message;
use App\Entity\Utilisateur;
use App\Form\Type\MessageType;
use App\Repository\MessageRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * @Route(name="back_message_", path="/back/message")
 */
class MessageController
{
    /**
     * @Route(name="liste", path="/liste")
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function list(
        Environment $twig,
        MessageRepository $messageRepository
    ): Response
    {
        $list = $messageRepository->findAll();
        $html = $twig->render('back/message/liste.html.twig', ['liste' => $list]);
        return new Response($html);
    }

    /**
     * @Route(name="create", path="/create")
     * @Route(name="update", path="/{message}", requirements={"message":"\d+"})
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function edite(
        Request $request,
        Environment $twig,
        EntityManagerInterface $em,
        Session $session,
        FormFactoryInterface $formFactory,
        Security $security,
        RouterInterface $router,
        EventDispatcher $dispatcher,
        Message $message = null
    ): Response
    {
        $new = false;
        $btLabel = 'Modifier';
        $valideMessage = 'Message modifier';
        if ($message === null) {
            $message = new Message();
            $message->setDateCreation(new DateTime());
            $message->setDateModification(new DateTime());
            /** @var Utilisateur $user */
            $user = $security->getUser();
            $message->setAuteur($user);
            $new = true;
            $btLabel = 'Ajouter';
            $valideMessage = 'Message ajouter';
        }

        $form = $formFactory->createBuilder(MessageType::class, $message)
            ->add($btLabel, SubmitType::class)
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($new) {
                $em->persist($message);
            }

            $em->flush();

            $session->getFlashBag()->add('info', $valideMessage);

            $url = $router->generate('back_message_liste');
            return new RedirectResponse($url);
        }


        $html = $twig->render('back/message/create.html.twig', [
            'form' => $form->createView()
        ]);
        return new Response($html);
    }

    /**
     * @Route(name="delete", path="/{message}/delete")
     */
    public function delete(
        Message $message,
        EntityManagerInterface $em,
        Session $session,
        RouterInterface $router
    ): Response
    {
        $em->remove($message);
        $em->flush();
        $session->getFlashBag()->add('info', 'Message supprimer');

        $url = $router->generate('back_message_liste');
        return new RedirectResponse($url);
    }
}