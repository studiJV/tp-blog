<?php


namespace App\Controller\Back;


use App\Form\Type\TagType;
use App\Repository\TagRepository;
use App\Entity\Tag;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * @Route(name="back_tag_", path="/back/tag")
 */
class TagController
{
    /**
     * @Route(name="liste", path="/liste")
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function list(
        Environment $twig,
        TagRepository $tagRepository
    ): Response
    {
        $list = $tagRepository->findAll();
        $html = $twig->render('back/tag/liste.html.twig', ['liste' => $list]);
        return new Response($html);
    }

    /**
     * @Route(name="create", path="/create")
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function create(
        Request $request,
        Environment $twig,
        EntityManagerInterface $em,
        Session $session,
        FormFactoryInterface $formFactory,
        RouterInterface $router
    ): Response
    {
        $tag = new Tag();
        $form = $formFactory->createBuilder(TagType::class, $tag)
            ->add('Ajouter', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $em->persist($tag);
            $em->flush();

            $session->getFlashBag()->add('info', 'Tag ajouter');
            $url = $router->generate('back_tag_liste');
            return new RedirectResponse($url);
        }


        $html = $twig->render('back/tag/create.html.twig', [
            'form' => $form->createView()
        ]);
        return new Response($html);
    }

    /**
     * @Route(name="delete", path="/{tag}/delete")
     */
    public function delete(
        Tag $tag,
        EntityManagerInterface $em,
        Session $session,
        RouterInterface $router
    ): Response
    {
        $em->remove($tag);
        $em->flush();
        $session->getFlashBag()->add('info', 'Tag supprimer');

        $url = $router->generate('back_tag_liste');
        return new RedirectResponse($url);
    }
}