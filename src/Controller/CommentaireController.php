<?php


namespace App\Controller;


use App\Entity\Commentaire;
use App\Entity\Message;
use App\Entity\Tag;
use App\Entity\Utilisateur;
use App\Form\Type\CommentaireType;
use App\Form\Type\TagType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class CommentaireController
{

    /**
     * @Route(name="add_commentaire", path="/commentaire/add/{message}", requirements={"message":"\d+"})
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function add(
        Request $request,
        EntityManagerInterface $em,
        Session $session,
        FormFactoryInterface $formFactory,
        RouterInterface $router,
        Security $security,
        Message $message
    ): Response
    {
        $commentaire = new Commentaire();
        $form = $formFactory->createBuilder(CommentaireType::class, $commentaire)
            ->add('Ajouter', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $commentaire->setMessage($message);
            /** @var Utilisateur $user */
            $user = $security->getUser();
            $commentaire->setAuteur($user);
            $commentaire->setDateCreation(new \DateTime());

            $em->persist($commentaire);
            $em->flush();

            $session->getFlashBag()->add('info', 'Commentaire ajouter');
        } else {
            $session->getFlashBag()->add('danger', 'Commentaire n\'a pas put être ajouter');
        }


        $url = $router->generate('message_detail', ['message'=> $message->getId()]);
        return new RedirectResponse($url);
    }
}