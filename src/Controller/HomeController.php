<?php


namespace App\Controller;


use App\Entity\Message;
use App\Repository\MessageRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class HomeController
{
    /**
     * @Route(name="home", path="/")
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function index(
        Request $request,
        PaginatorInterface $paginator,
        Environment $twig,
        MessageRepository $messageRepository
    ): Response
    {
        /** @var Message[] $list */
        $list = $paginator->paginate(
            $messageRepository->getMessagePublier(),
            $request->query->getInt('page', 1),
            5
        );

        $html = $twig->render('home/index.html.twig', ['liste' => $list]);
        return new Response($html);
    }
}