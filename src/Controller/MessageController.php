<?php


namespace App\Controller;


use App\Entity\Message;
use App\Form\Type\CommentaireType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class MessageController
{
    /**
     * @Route(name="message_detail", path="/{message}", requirements={"message":"\d+"})
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function detail(
        Environment $twig,
        FormFactoryInterface $formFactory,
        Message $message,
        RouterInterface $router
    ): Response
    {

        $form = $formFactory->createBuilder(CommentaireType::class)
            ->setAction($router->generate('add_commentaire', ['message' => $message->getId()]))
            ->add('Ajouter', SubmitType::class)
            ->getForm();

        $html = $twig->render('message/detail.html.twig', ['message' => $message, 'form' => $form->createView()]);
        return new Response($html);
    }
}