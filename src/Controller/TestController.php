<?php


namespace App\Controller;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestController
{
    /**
     * @Route(name="test_public", path="/test")
     */
    public function publicTest()
    {
        return new Response('<html><body>public<br/><a href="/back/test">admin</a> <br/></body></html>');
    }

    /**
     * @Route(name="test_back", path="/back/test")
     */
    public function backTest()
    {
        return new Response('<html><body>Back </body></html>');
    }
}