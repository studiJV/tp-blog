<?php


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Commentaire
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="Message", inversedBy="commentaires")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private Message $message;

    /**
     * @ORM\Column(type="text")
     */
    private string $contenue;

    /**
     * @ORM\ManyToOne(targetEntity="Utilisateur", inversedBy="commentaires")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private Utilisateur $auteur;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTime $dateCreation;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Commentaire
     */
    public function setId(int $id): Commentaire
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Message
     */
    public function getMessage(): Message
    {
        return $this->message;
    }

    /**
     * @param Message $message
     * @return Commentaire
     */
    public function setMessage(Message $message): Commentaire
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getContenue(): string
    {
        return $this->contenue;
    }

    /**
     * @param string $contenue
     * @return Commentaire
     */
    public function setContenue(string $contenue): Commentaire
    {
        $this->contenue = $contenue;
        return $this;
    }

    /**
     * @return Utilisateur
     */
    public function getAuteur(): Utilisateur
    {
        return $this->auteur;
    }

    /**
     * @param Utilisateur $auteur
     * @return Commentaire
     */
    public function setAuteur(Utilisateur $auteur): Commentaire
    {
        $this->auteur = $auteur;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreation(): \DateTime
    {
        return $this->dateCreation;
    }

    /**
     * @param \DateTime $dateCreation
     * @return Commentaire
     */
    public function setDateCreation(\DateTime $dateCreation): Commentaire
    {
        $this->dateCreation = $dateCreation;
        return $this;
    }
}