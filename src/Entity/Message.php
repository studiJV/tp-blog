<?php


namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Message
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string")
     */
    private string $titre;

    /**
     * @ORM\Column(type="string")
     */
    private string $slug;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTime $dateCreation;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTime $dateModification;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    private ?\DateTime $datePublication;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $resumer;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $contenu;

    /**
     * @ORM\ManyToOne(targetEntity="Utilisateur", inversedBy="messages")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private Utilisateur $auteur;

    /**
     * @ORM\ManyToMany(targetEntity="Tag", inversedBy="messages")
     * @ORM\JoinTable()
     */
    private Collection $tags;

    /**
     * @var Collection|Commentaire[]
     * @ORM\OneToMany(targetEntity="Commentaire", mappedBy="message")
     */
    private Collection $commentaires;


    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->commentaires = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Message
     */
    public function setId(int $id): Message
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitre(): string
    {
        return $this->titre;
    }

    /**
     * @param string $titre
     * @return Message
     */
    public function setTitre(string $titre): Message
    {
        $this->titre = $titre;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return Message
     */
    public function setSlug(string $slug): Message
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreation(): \DateTime
    {
        return $this->dateCreation;
    }

    /**
     * @param \DateTime $dateCreation
     * @return Message
     */
    public function setDateCreation(\DateTime $dateCreation): Message
    {
        $this->dateCreation = $dateCreation;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateModification(): \DateTime
    {
        return $this->dateModification;
    }

    /**
     * @param \DateTime $dateModification
     * @return Message
     */
    public function setDateModification(\DateTime $dateModification): Message
    {
        $this->dateModification = $dateModification;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDatePublication(): ?\DateTime
    {
        return $this->datePublication;
    }

    /**
     * @param \DateTime|null $datePublication
     * @return Message
     */
    public function setDatePublication(?\DateTime $datePublication): Message
    {
        $this->datePublication = $datePublication;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getResumer(): ?string
    {
        return $this->resumer;
    }

    /**
     * @param string|null $resumer
     * @return Message
     */
    public function setResumer(?string $resumer): Message
    {
        $this->resumer = $resumer;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getContenu(): ?string
    {
        return $this->contenu;
    }

    /**
     * @param string|null $contenu
     * @return Message
     */
    public function setContenu(?string $contenu): Message
    {
        $this->contenu = $contenu;
        return $this;
    }

    /**
     * @return Utilisateur
     */
    public function getAuteur(): Utilisateur
    {
        return $this->auteur;
    }

    /**
     * @param Utilisateur $auteur
     * @return Message
     */
    public function setAuteur(Utilisateur $auteur): Message
    {
        $this->auteur = $auteur;
        return $this;
    }

    /**
     * @return ArrayCollection|Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param ArrayCollection|Collection $tags
     * @return Message
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * @return Commentaire[]|Collection
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }

    /**
     * @param Commentaire[]|Collection $commentaires
     * @return Message
     */
    public function setCommentaires($commentaires)
    {
        $this->commentaires = $commentaires;
        return $this;
    }
}