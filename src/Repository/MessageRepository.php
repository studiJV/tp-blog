<?php


namespace App\Repository;


use App\Entity\Message;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class MessageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Message::class);
    }

    public function findAll(): array
    {
        return $this->findBy([], ['datePublication' => 'DESC']);
    }

    public function getMessagePublier()
    {
        $querry = $this->createQueryBuilder('m')
            ->orderBy('m.datePublication ','DESC')
            ->where( 'm.datePublication < CURRENT_DATE()');
        return $querry->getQuery();
    }
}